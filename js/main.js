(function ($) {
  Drupal.behaviors.exampleModule = {
    attach: function (context, settings) {

$(document).ready(function() {
  var $grid = $('.grid').isotope({
  itemSelector: '.grid-item',
  layoutMode: 'masonry'
});
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});
$('.btn-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on('click','button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});
$(".toggle-accordion").on("click", function() {
    var accordionId = $(this).attr("accordion-id"),
      numPanelOpen = $(accordionId + ' .collapse.in').length;    
    $(this).toggleClass("active");
    if (numPanelOpen == 0) {
      openAllPanels(accordionId);
    } else {
      closeAllPanels(accordionId);
    }
  })
  openAllPanels = function(aId) {
    console.log("setAllPanelOpen");
    $(aId + ' .panel-collapse:not(".in")').collapse('show');
  }
  closeAllPanels = function(aId) {
    console.log("setAllPanelclose");
    $(aId + ' .panel-collapse.in').collapse('hide');
  }
$('.page-node-type-our-physicians ul.menu').each(function() {
    $("li a", this).each(function(i) {
        if($(this).text()==="About Us")
        $(this).addClass('act');
    });
});
$('.carousel[data-type="multi"] .item').each(function(){
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));    
    for (var i=0;i<1;i++) {
      next=next.next();
      if (!next.length) {
          next = $(this).siblings(':first');
      }      
      next.children(':first-child').clone().appendTo($(this));
    }
  });
 $("#navbar-header-top").animate({height:40},200); $('#fullpage').fullpage({ anchors: ['sections1', 'sections2', 'sections3', 'sections4', 'sections5', 'sections6', 'sections7', 'sections8', 'sections9'], menu: '#menu-links', navigation: true, css3: false, scrollOverflow: true}); $('#navbar-header-top .close').click(function(){ $("#navbar-header-top").animate({height:0},200); }); $(".menu li.dropdown a").addClass("disabled"); $('.ba-slider').each(function() { var cur = $(this); var width = cur.width()+'px'; cur.find('.resize img').css('width', width); drags(cur.find('.handle'), cur.find('.resize'), cur); });
 $('#block-webform-3').attr("data-spy", "affix"); $('#block-webform-3').attr("data-offset-top", "540"); $('body.user-logged-in.path-node.page-node-type-page').addClass("bsak");
    if($("#navbar-collapse").is(".topr")) {
    var tle = $(".menu.dropdown-menu li");
    for(var i = 0; i < tle.length; i+=4) {
      tle.slice(i, i+4)
         .wrapAll("<div class='col-md-3 proc-list-cont text-left'><ul class='proc-list'></ul></div>");
    }      
    $('.menu.dropdown-menu>ul').unwrap();
    $("ul.menu.dropdown-menu").wrapInner( "<div class='container'></div>");
    $("#navbar-collapse").removeClass("topr");
  };
  $(".dropdown").hover(function(){ $(this).addClass('open')},function(){ $(this).removeClass('open')});
var url = $(location).attr('href');
var parts = url.split("/");
var last_part = parts[parts.length-1];
$('#proc ul').each(function() {
    $("li", this).each(function(i) {
        if(this.className===last_part)
        $(this).children("a").addClass('act');
    });
});
function drags(dragElement, resizeElement, container) {
  dragElement.on('mousedown touchstart', function(e) {    
    dragElement.addClass('draggable');
    resizeElement.addClass('resizable');
    var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;
    var dragWidth = dragElement.outerWidth(),
        posX = dragElement.offset().left + dragWidth - startX,
        containerOffset = container.offset().left,
        containerWidth = container.outerWidth();
    minLeft = containerOffset + 10;
    maxLeft = containerOffset + containerWidth - dragWidth - 10;
    dragElement.parents().on("mousemove touchmove", function(e) {
      var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;
      leftValue = moveX + posX - dragWidth;
      if ( leftValue < minLeft) {
        leftValue = minLeft;
      } else if (leftValue > maxLeft) {
        leftValue = maxLeft;
      }
      widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
      $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
        $(this).removeClass('draggable');
        resizeElement.removeClass('resizable');
      });
      $('.resizable').css('width', widthValue);
    }).on('mouseup touchend touchcancel', function(){
      dragElement.removeClass('draggable');
      resizeElement.removeClass('resizable');
    });
    e.preventDefault();
  }).on('mouseup touchend touchcancel', function(e){
    dragElement.removeClass('draggable');
    resizeElement.removeClass('resizable');
  });
}
}); 

}
};
}(jQuery));